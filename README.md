This repository contains the source for the website https://krr.gitlab.io/demos

To update the website:

* commit your changes to your local repository
* push to gitlab.com/krr/demos

The website will be automatically updated after a couple of minutes (track progress on Gitlab / CI/CD / Pipelines)
