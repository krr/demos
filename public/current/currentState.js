window.idpCode = $.ajax( {
    url : "./desiredPre.idp",
    async : false
}).responseText;

var formApp = angular.module('formCurrent', ['schemaForm','ui.sortable']);
formApp.controller('FormController', function($scope) {
    var scenarios = [
        {
            "log" : [],
            "curTime" : 1,
            "desired" : []
        },
        {
            "log": [
                {
                    "eventType": "Release",
                    "software": "Norton",
                    "version": 1
                },
                {
                    "eventType": "Release",
                    "software": "Notepad",
                    "version": 1
                },
                {
                    "eventType": "Install",
                    "software": "Notepad",
                    "version": 1,
                    "pc": "PC"
                }
            ],
            "curTime": 3,
            "desired": [

            ]
        },
        {
            "log": [
                {
                    "eventType": "Release",
                    "software": "AVG",
                    "version": 1
                },
                {
                    "eventType": "Install",
                    "pc": "PC1",
                    "software": "AVG",
                    "version": 1
                },
                {
                    "eventType": "Install",
                    "pc": "PC2",
                    "software": "AVG",
                    "version": 1
                },
                {
                    "eventType": "Release",
                    "software": "AVG",
                    "version": 2
                },
                {
                    "eventType": "Install",
                    "pc": "PC1",
                    "software": "AVG",
                    "version": 2
                }
            ],
            "curTime": 5,
            "desired": [
                "! pc s v : installed(pc,s,v) => latest(s)=v",
                "! pc s : ? v : installed(pc,s,v)"
            ]
        }

    ];

    $scope.loadScenario = function(i){
        $scope.model = scenarios[i];
    };

    $scope.schema = {
        "type": "object",
        "title": "Current State Log",
        "properties": {
            "curTime": {
                "title": "Requested Timepoint",
                "type": "number"
            },
            "log": {
                "title" : "Current State History",
                "type": "array",
                "items": {
                    "type": "object",
                    "title" : "History Entry",
                    "properties": {
                        "eventType": {
                            "title": "Type of Event",
                            "type": "string",
                            "enum": [
                                "Release",
                                "Install"
                            ]
                        },
                        "software": {
                            "title": "Software Name",
                            "type": "string"
                        },
                        "version": {
                            "title": "Software Version",
                            "type": "number"
                        },
                        "pc": {
                            "title": "PC Name",
                            "type": "string"
                        }
                    }
                }
            },
            "desired" : {
                "title" : "Desired State Properties",
                "type" : "array",
                "items" : {
                    "type" : "string",
                    "title" : "Desired State Property"
                }
            }
        },
        "required": []
    };

    $scope.form =
        [
            "curTime",
            {
                "key": "log",
                "items": [
                    {
                        "type": "fieldset",
                        "htmlClass": "row",
                        "items": [
                            {
                                "type": "fieldset",
                                "htmlClass": "col-md-4",
                                "items": [
                                    {
                                        "key": "log[].eventType",
                                        "type": "radiobuttons",
                                        "notitle" : true
                                    }
                                ]
                            },
                            {
                                "type": "fieldset",
                                "htmlClass": "col-md-4",
                                "items": [
                                    {
                                        "key": "log[].pc",
                                        "notitle" : true,
                                        "placeholder" : "My PC",
                                        "condition": "model.log[arrayIndex].eventType == 'Install'"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "type": "fieldset",
                        "htmlClass": "row",
                        "items": [
                            {
                                "type": "fieldset",
                                "htmlClass": "col-md-4",
                                "items": [
                                    {
                                        "key" : "log[].software",
                                        "placeholder" : "IDP",
                                        "notitle" : true
                                    }
                                ]
                            },
                            {
                                "type": "fieldset",
                                "htmlClass": "col-md-3",
                                "items": [
                                    {
                                        "key" : "log[].version",
                                        "notitle" : true,
                                        "placeholder" : "3"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "key": "desired",
                "items" : [
                    {
                        "key" : "desired[]",
                        "notitle" : true,
                        "placeholder" : "IDP code for desired property goes here"
                    }
                ]


            }
        ];

    $scope.model =   {"log":[],"curTime":1, "desired" : []};
    $scope.visu = {};
    $scope.makeTheo = function () {
        var out = "theory T2 : V {\n";
        for(var i in $scope.model.desired){
            out += $scope.model.desired[i] + ".\n"
        }
        out += "}";
        return out;
    };
    $scope.submit = function(){
        var inp = JSON.stringify($scope.model);
        console.log(inp);
        var r = {"code" : window.idpCode + $scope.makeTheo(),
            "GV" : 0,
            "SV" : 0,
            "NB" : 0,
            "inference" : 2,
            "stable" : false,
            "cp" : false,
            "filename" : "main",
            "eInput" : inp
        };
        $.ajax({
            type: 'POST',
            url: "https://idp.cs.kuleuven.be/idp/eval",
            data: JSON.stringify(r),
            async : false
        }).done(function(res){
            console.log(res.stderr);
            console.log(res.stdout);
            if(sanitize(res.stdout) != ""){
                $scope.visu = JSON.parse(sanitize(res.stdout));
                $scope.err = res.stderr;
                $scope.showErrors = false;
                console.log($scope.visu.tmp)
            }else{
                $scope.visu = {};
                $scope.err = res.stderr;
            }
            if(!$scope.visu){
                $scope.showErrors = true;
            }
        });

    };
});

function sanitize(s) {
  if(s.indexOf("{") >= 0){
    return s.substring(s.indexOf("{"));
  } else if(s.indexOf("[") >= 0){
    return s.substring(s.indexOf("["));
  } else {
    s
  }
}
