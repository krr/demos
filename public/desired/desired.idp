include "idpd3/idpd3_voc.idp"


vocabulary V{
    type PC isa string
    type Software isa string

    antivirus(Software)

    type Version isa int
    type Cost isa int

    dependsOn(Software,Software)
    incompatible(Software,Software)

    current(PC,Software)
    desired(PC,Software)

    install(PC,Software)
    installCost : Cost

    deinstall(PC,Software)
    deinstallCost : Cost

}

//Main theory with the hard dependencies
theory T: V{

    {
        ! pc s : desired(pc,s) <- current(pc,s) & ~deinstall(pc,s).
        ! pc s : desired(pc,s) <- install(pc,s).
    }

    ! pc s s2 : desired(pc,s) & dependsOn(s,s2) => desired(pc,s2).
    ! pc s s2 : (desired(pc,s) & desired(pc,s2)) => ~incompatible(s,s2).


}


term cost : V {
    #{pc s : install(pc,s)}*installCost +
    #{pc s : deinstall(pc,s)}*deinstallCost
}


procedure toKey(a){
    return tostring(a)
}


procedure toLink(a,b){
    return tostring(a).."-"..tostring(b)
}

vocabulary V_d3 {
    extern vocabulary idpd3::V_out
    extern vocabulary V

    softToKey(Software):key
    softToLabel(Software):label
    pcToKey(PC):key
    pcToLabel(PC):label
    keyToLabel(key):label
    linkToKey(PC,Software):key
}
theory T_d3 : V_d3 {
    {
        d3_type(1,pcToKey(pc)) = text.
        d3_text_label(1,pcToKey(pc)) = pcToLabel(pc).
        d3_text_size(1,pcToKey(pc)) = 3.
        d3_color(1,pcToKey(soft)) = "white".
        d3_node(1, pcToKey(pc)).

        d3_type(1,softToKey(soft)) = text.
        d3_text_label(1,softToKey(soft)) = softToLabel(soft).
        d3_text_size(1,softToKey(soft)) = 3.
        d3_color(1,softToKey(soft)) = "yellow".
        d3_node(1, softToKey(soft)).

        d3_type(1, linkToKey(pc,s)) = link <- desired(pc,s) | deinstall(pc,s).
        d3_link_from(1, linkToKey(pc,s)) = pc <- desired(pc,s) | deinstall(pc,s).
        d3_link_to(1, linkToKey(pc,s)) = s <- desired(pc,s) | deinstall(pc,s).
        d3_link_width(1, linkToKey(pc,s)) = 2  <- desired(pc,s) | deinstall(pc,s).

        d3_color(1, linkToKey(pc,s)) = "grey" <- current(pc,s) & desired(pc,s).
        d3_color(1, linkToKey(pc,s)) = "green" <- ~current(pc,s) & desired(pc,s).
        d3_color(1, linkToKey(pc,s)) = "red" <- deinstall(pc,s).

        d3_width(t) = MAX[:width].
        d3_height(t) = MAX[:height].

        d3_rect_width(t, x) = y <- false.
        d3_rect_height(t, x) = y <- false.
        d3_x(t, x) = y <- false.
        d3_y(t, x) = y <- false.
        d3_circ_r(t, x) = y <- false.
    }
}

structure SI : V_d3{
    Software = {Norton; Avast; AVG; Calculator ; Paint}
    antivirus = {Norton ; Avast ; AVG}
    PC = {PC1 ; PC2}


    installCost = 5
    deinstallCost = 1

    time = {1}

    color = {"white" ; "black" ; "yellow" ; "grey" ; "red" ; "green" ; "purple"}
    width = {0..75}
    height = {0..75}

    softToKey = procedure toKey
    pcToKey = procedure toKey

    linkToKey = procedure toLink
    keyToLabel = procedure toKey

    softToLabel = procedure toKey
    pcToLabel = procedure toKey
}

procedure main(){

    idpd3.init_idpd3();
    stdoptions.cpsupport = true

    local SS,a,cost = minimize(merge(T_d3,merge(T,User)),SI,cost)
    idpd3.visualise(SS[1])
}
