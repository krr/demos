window.idpCode = $.ajax( {
    url : "./idpCode.idp",
    async : false
}).responseText



// window.vocAndProc = $.ajax( {
//     url : "./vocAndProc.idp",
//     async : false
// }).responseText
//
// window.theorie = $.ajax( {
//     url : "./theorie.idp",
//     async : false
// }).responseText
//
// window.structuur = $.ajax( {
//     url : "./structuur.idp",
//     async : false
// }).responseText

var app = angular.module('myApp', ['ui.bootstrap','ui.codemirror', 'ngRoute']);

app.controller('sheet', function($scope, $parse, $uibModal, $route,$window) {
  $scope.columns = ['A', 'B', 'C', 'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V'];

  $scope.rows = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];

  //Een object met de waarde die in de cell is geschreven door de gebruiker
  $scope.cells = {};

  $scope.textCells = {};

  $scope.spreadsheetText = [];
  $scope.spreadsheetNumber = [];

  //Een object met id de cel en als waarde de definitie die in IDP met komen
  $scope.IDPDef = {};

  var isNotEmptySpreadsheetPredicate = false;

  //Een object met als id predicaat naam en als waarde de definitie ervan
  $scope.predicateDef = {};

  //Een object met id de cel en als waarde de waarde die IDP als uitkomst voor deze cel geeft
  $scope.Defmodel = {};

  //Een lijst met alle predicaten (hele objecten)
  $scope.predicates = [];

  //Een object van de waarde van een cell dat van een predicaat is
  $scope.valueOfPredicateCell = {};

  //Een lijst met alle types
  //$scope.types = [];

  //Een lijst met alle cellen die behoren tot predicaten (zonder de titels)
  $scope.cellsOfPredicates = []

  //Een object met id de predicaat naam en als waarde een lijst met alle cellen van dit predicaat
  $scope.predicateLink = {};

  $scope.cellStyle = {};

  $scope.collSpan = {};

  //Aanpassing
  $scope.idpCode = [window.idpCode];
  //$scope.idpCode = [window.vocAndProc + window.theorie + window.structuur]

  $scope.constraints = "";

  $scope.requestSend = "Idle";

  $scope.numberOfModels = 0;
  $scope.modelAsked = 1;

  $scope.multipleModelsEnabled = true;
  $scope.referencesEnabled = true;

  var waitingList = {};

  process = function(exp) {
    return exp.replace(/[A-Z]+\d+/g, function(ref) {
      return 'compute("' + ref + '")';
    })
  }

  getIDPDefsAsString = function() {
    var def = "";

    if ($scope.referencesEnabled) {
      for (var p in $scope.predicates) {
        predicate = $scope.predicates[p];
        // ! a b : SpreadsheetT(y,rCol1)=a <- Pr(a,b,y).
        for (var i = 0 ; i < predicate.nbArguments; i++) {
          //Die a en b zijn ni altijd hetzelfde voor dat aantal argumenten.
          if (predicate["argumentType"+ i] == "SomeText") {
            def = def + "!";
            for (var j = 0 ; j < predicate.nbArguments; j++)
              def = def + String.fromCharCode(97 + j) + " ";
            def = def + ": CellT(y," + predicate.name + "Col" + (i+1) + ")=" + String.fromCharCode(97 + i) + " <- " + predicate.name + "Pos(";
            for (var j = 0 ; j < predicate.nbArguments; j++)
              def = def + String.fromCharCode(97 + j) + ",";
            def = def + "y) & (y < "  + predicate.name + "StartRow +" + predicate.length + ").\n\t";
          }
          else {
            def = def + "!";
            for (var j = 0 ; j < predicate.nbArguments; j++)
              def = def + String.fromCharCode(97 + j) + " ";
            def = def + ": Cell(y," + predicate.name + "Col" + (i+1) + ")=" + String.fromCharCode(97 + i) + " <- "+ predicate.name + "Pos(";
            for (var j = 0 ; j < predicate.nbArguments; j++)
              def = def + String.fromCharCode(97 + j) + ",";
            def = def + "y) & (y < "  + predicate.name + "StartRow +" + predicate.length + ").\n\t";
          }
        }

      }
    }

    for (var cell in $scope.IDPDef) {
      if ($scope.IDPDef.hasOwnProperty(cell) && $scope.IDPDef[cell] != undefined) {
        def = def + $scope.IDPDef[cell] + "\n\t";
      }
    }

    for (var predicate in $scope.predicateDef) {
      if ($scope.predicateDef.hasOwnProperty(predicate) && $scope.predicateDef[predicate] != undefined && $scope.predicateDef[predicate] != "") {
          def = def + $scope.predicateDef[predicate] + "\n\t";
      }

    }
    return def;
  }

  getSpreadsheetConstraintsAsString = function() {
    temp = "";
    temp2 = "";
    temp3 = "";
    //! a b r: Pr(a,b,r) => rStartRow =< r < rStartRow + #{y z : r(y,z)}.
      for (var i in $scope.predicates) {
        //for (var j in $scope.predicateDef)  { //TerugZetten als het ni werkt
          pred = $scope.predicates[i];
        //  if ($scope.predicateDef[j] != undefined && j == pred.name) { //terugzetten als het ni werkt
            temp = temp + "! ";
            temp2 = temp2 + "! ";
            temp3 = temp3 + "! ";
            for (var k = 0; k < pred.nbArguments; k++) {
              temp = temp + String.fromCharCode(97 + k) + " ";
              temp2 = temp2 + String.fromCharCode(97 + k) + " ";
              temp3 = temp3 + String.fromCharCode(97 + k) + "1 " +  String.fromCharCode(97 + k) + "2 ";
            }
            temp2 = temp2 + "r: " + pred.name + "Pos("

            temp3 = temp3 + "r1 r2: ("
            if (pred.nbArguments > 0)
              temp3 = temp3 + "(" + String.fromCharCode(97) + "1 ~= " + String.fromCharCode(97) + "2";
            for (var k = 1; k < pred.nbArguments - 1; k++) {
              temp3 = temp3 + " | " + String.fromCharCode(97 + k) + "1 ~= " + String.fromCharCode(97 + k) + "2";
            }
            if (pred.nbArguments > 1 )
              temp3 = temp3 + " | " + String.fromCharCode(97 + pred.nbArguments -1) + "1 ~= " + String.fromCharCode(97 + pred.nbArguments-1) + "2)";
            else if (pred.nbArguments > 0) {
                temp3 = temp3 + ")"
            }
            temp3 = temp3 + " & " + pred.name + "Pos(";
          //  temp3 = temp3 + "r1 r2: " + "((a1 ~= a2 | b1 ~= b2) & " + pred.name + "Pos(";
            temp = temp + ": " + pred.name + "(";
            for (var j = 0; j < pred.nbArguments - 1; j++) {
              temp = temp + String.fromCharCode(97 + j) + ",";
              temp2 = temp2 + String.fromCharCode(97 + j) + ",";
              temp3 = temp3 + String.fromCharCode(97 + j) + "1,";
            }
            temp = temp + String.fromCharCode(97 + pred.nbArguments - 1) + ") <=> (? r : " + pred.name + "Pos(";
            temp2 = temp2 + String.fromCharCode(97 + pred.nbArguments - 1) + ",r) => " + pred.name + "StartRow =< r <" + pred.name + "StartRow + #{"
            temp3 = temp3 + String.fromCharCode(97 + pred.nbArguments - 1) + "1,r1) & " + pred.name + "Pos(";
            for (var l = 0; l < pred.nbArguments; l++) {
              temp = temp + String.fromCharCode(97 + l) + ",";
              temp2 = temp2 + String.fromCharCode(110 + l) + " "
              temp3 = temp3 + String.fromCharCode(97 + l) + "2,";
            }
            temp = temp + "r)). \n\t";
            temp2 = temp2 + ": " + pred.name + "("
            temp3 = temp3 + "r2)) => r1 ~= r2.\n\t";
            for (var j = 0; j < pred.nbArguments -1 ; j++)
              temp2 = temp2 + String.fromCharCode(110 + j) + ","
            temp2 = temp2 + String.fromCharCode(110 + pred.nbArguments - 1) + ")}.\n\t"//")} & r < " + pred.name + "StartRow +"  + pred.length + ".\n\t"
        //  } //
      //  } //
      }
      return temp + temp2 + temp3;
  }

  getPredicatesAsString = function() {
    var predicates = "";
    var PosPredicates = "";
    /*
    for (var i in $scope.types) {
      predicates = predicates + "type " + $scope.types[i] + "\n\t"
    }
    */
    for (var i in $scope.predicates) {
      var predicate = $scope.predicates[i];
      var columns = "";
      predicates = predicates + predicate.name + "(";
      PosPredicates = PosPredicates + predicate.name + "Pos(";
      for(var i = 0; i < predicate.nbArguments - 1; i++) {
        predicates = predicates + predicate["argumentType" + i] + ",";
        PosPredicates= PosPredicates + predicate["argumentType" + i] + ",";
        columns = columns + predicate.name + "Col" + (i + 1) + ": Column\n\t";
      }
      PosPredicates = PosPredicates + predicate["argumentType" + (predicate.nbArguments - 1)] + ",Row" + ")\n\t";
      columns = columns +  predicate.name + "Col" + predicate.nbArguments + ": Column\n\t"
      PosPredicates = PosPredicates + predicate.name + "StartRow: Row" + "\n\t" + columns;
      predicates = predicates + predicate["argumentType" + (predicate.nbArguments - 1)] + ")\n\t";

    }
    if($scope.referencesEnabled)
      return predicates + PosPredicates;
    else
      return predicates;

  }


  isCellOfType = function(cell, type) {
    for (var i in $scope.predicates) {
      predicate = $scope.predicates[i];
      name = predicate.name;
      if($.inArray(cell, $scope.predicateLink[name]) == -1 ) {
        continue;
      }
      typeNummer = cell.charCodeAt(0) - predicate.cell1.charCodeAt(0);
      return predicate["argumentType" + typeNummer] == type;
    }
    return false;
  }

  getStringsAsString = function() {
    types = "SomeText = {";
    typesValues = [];
    for(j in $scope.cellsOfPredicates) {
      cell = $scope.cellsOfPredicates[j];
      if (isCellOfType(cell, "SomeText"))
        if ($scope.valueOfPredicateCell[cell] != null && $.inArray($scope.valueOfPredicateCell[cell],typesValues) == -1) {
          typesValues.push($scope.valueOfPredicateCell[cell]);
          types = types + '"' + $scope.valueOfPredicateCell[cell] + '"' + ";";
        }
    }
    for(var i in $scope.textCells) {
        if ($scope.textCells[i] != null && $.inArray($scope.textCells[i],typesValues) == -1) {
          typesValues.push($scope.textCells[i]);
          types = types + '"' + $scope.textCells[i] + '"' + ";";
        }
    }
    return types + "}";
  }

  getProceduresAsString = function() {
    proc = "";
    for (i in $scope.predicates) {
      predicate = $scope.predicates[i];
      name = predicate.name;
      nb = predicate.nbArguments;
      temp = " for i in tuples(S2[V::" + name + "].ct) do\n\t\t\ttable.insert(output,{name = " + '"' + name + '"';
      for (var j = 0; j < nb; j++) {
        k = j+1;
        temp = temp + ", value" + j + " = i[" + k + "]";
      }
      temp = temp + "})\n\t\t end\n\n\t\t ";
      proc = proc + temp;
    }
    return proc;
  }

  getValueOfPredicatesAsString = function() {
    pred = "";
    pred2 = "";
    for (i in $scope.predicates) {
      predicate = $scope.predicates[i];
      name = predicate.name;
      startRow = parseInt(predicate.cell1.substring(1)) + 2;
      predString = name + "={"
      PredPos =  name + "Pos={"
      cells = $scope.predicateLink[name];
      nb = parseInt(predicate.nbArguments);
      counter = 0;
      elementCounter = 0;
      check = true;
      aValue = false;
      temp = "(";
      PosTemp = "("
      for (j in cells) {
        cellValue = $scope.valueOfPredicateCell[cells[j]];
        if (cellValue == null)
          check = false;

        if (counter < nb-1) {
          temp = temp + '"' + cellValue + '"' + ",";
          PosTemp = PosTemp + '"' + cellValue + '"' +  ",";
          counter++;
        }
        else if (counter == nb-1 && check) {
          temp = temp + '"' + cellValue + '"' + ");"
          PosTemp = PosTemp + '"' + cellValue + '"' + "," +(startRow+elementCounter) +  ");";
          predString = predString + temp;
          PredPos = PredPos + PosTemp;
          aValue = true;
          temp = "(";
          PosTemp = "(";
          counter = 0;
          elementCounter++;
          check = true;
        }
        else {
          PosTemp = "(";
          temp = "(";
          counter = 0;
          elementCounter = 0;
          check = true;
        }
      }
        predString = predString + "}\n\t";
        PredPos = PredPos + "}\n\t";
        if (aValue) {
          pred = pred + predString ;
          pred2 = pred2 + PredPos ;
        }


    }

    if($scope.referencesEnabled)
      return pred  + pred2;
    else
      return pred;
  }

  getStructureHelperPredicatesAsString = function() {
    temp = "";
    for (var i in $scope.predicates) {
      temp = temp + $scope.predicates[i].name + "StartRow = " + (parseInt($scope.predicates[i].cell1.substring(1)) + 2)  + "\n\t";
      for (var j = 0; j < $scope.predicates[i].nbArguments; j++) {
        temp = temp + $scope.predicates[i].name + "Col" + (j+1) + " = \"" + String.fromCharCode($scope.predicates[i].cell1.charCodeAt(0) + j) + "\"\n\t"
      }
    }
    return temp;
  }

  putDefsInCode = function() {
      //return $scope.idpCode[0].replace(/cell definities$\n^\s*{$\n(\s|[^}])*}/m,"hih")
      return $scope.idpCode[0].replace(/cell definities(\r\n|\n|\r)^\s*{(\r\n|\n|\r)(\s|[^}])*}/m, getIDPDefsAsString());
  }
  //cell definities\r\n    {\r\n        \r\n    }

  putPredicatesInCode = function() {
    return $scope.idpCode[0].replace(/\/\/gelinkte predicaten101010/m,getPredicatesAsString());
  }


  PutLinksIncode = function() {
      tempCode = $scope.idpCode[0].replace(/\/\/cell definities101010/m, getIDPDefsAsString());
      temp2Code = tempCode.replace(/\/\/Types toevoegen101010 SomeText = {}/m,getStringsAsString());
      temp3Code = temp2Code.replace(/\/\/structuur elementen toevoegen101010/m,getValueOfPredicatesAsString());
      //temp4Code = temp3Code.replace(/\/\/proc toevoegen101010/m,getProceduresAsString());
      temp5Code = temp3Code.replace(/\/\/constraints toevoegen101010/m,$scope.constraints);
      if ($scope.referencesEnabled) {
        temp6Code = temp5Code.replace(/\/\/hulp elementen toevoegen101010/m,getStructureHelperPredicatesAsString())
        temp7Code = temp6Code.replace(/\/\/Spreadsheet constraints toevoegen101010/m, getSpreadsheetConstraintsAsString())
      }
      else {
        temp7Code = temp5Code.replace(/\/\/proc toevoegen101010/m,getProceduresAsString());
      }
      return temp7Code.replace(/\/\/gelinkte predicaten101010/m,getPredicatesAsString());
  }

  $scope.transitiveClosure = function() {
    //Mss eerst het predicaat die in één van deze cellen zitten op dit moment weg doen ?
    //met de functie deletePredicate(); eerst zoeken naar predicaat dat deze cellen gebruikt.
      resetVars();
      predicate1 = {name: "r", nbArguments: 2, length: 8, cell1: "A1", cell2: "B10", argument0: "arg1", argument1: "arg2", argumentType0: "SomeText", argumentType1: "SomeText" };
      predicate2 = {name: "tcr", nbArguments: 2, length: 8, cell1: "D1", cell2: "E10", argument0: "arg1", argument1: "arg2", argumentType0: "SomeText", argumentType1: "SomeText" };
      initialisePredicate(predicate1);
      linkPToCells(predicate1);
      $scope.predicates.push(predicate1);
      initialisePredicate(predicate2);
      linkPToCells(predicate2);
      $scope.predicates.push(predicate2);

      $scope.cells["A3"] = "a";
      evaluatePredicate("A", 3,"a");
      $scope.cells["B3"] = "b";
      evaluatePredicate("B", 3,"b");
      $scope.cells["A4"] = "c";
      evaluatePredicate("A", 4,"c");
      $scope.cells["B4"] = "d";
      evaluatePredicate("B", 4,"d");
      $scope.cells["A5"] = "e";
      evaluatePredicate("A", 5,"e");
      $scope.cells["B5"] = "f";
      evaluatePredicate("B", 5,"f");
      $scope.cells["A6"] = "b";
      evaluatePredicate("A", 6,"b");
      $scope.cells["B6"] = "g";
      evaluatePredicate("B", 6,"g");

      $scope.predicateDef["tcr"] = "! a1 a2: tcr(a1, a2) <- r(a1, a2).\n! a1 a3: tcr(a1, a3) <- ? a2: r(a1, a2) & tcr(a2, a3)."
      $scope.request();
      $scope.reloadRoute;

  }

  $scope.nurseScheduling = function() {
      resetVars();
      predicate1 = {name: "Nurse", nbArguments: 1, length: 6, cell1: "A1", cell2: "A6", argument0: "name", argumentType0: "SomeText"};
      predicate2 = {name: "Shift", nbArguments: 1, length: 3, cell1: "A8", cell2: "A12", argument0: "type", argumentType0: "SomeText"};
      predicate3 = {name: "Day", nbArguments: 1, length: 7, cell1: "C1", cell2: "C9", argument0: "day", argumentType0: "SomeText"};
      predicate4 = {name: "Work", nbArguments: 3, length: 21, cell1: "E1", cell2: "G23", argument0: "nurse", argumentType0: "SomeText", argument1: "shift", argumentType1: "SomeText", argument2: "day", argumentType2: "SomeText"};
      initialisePredicate(predicate1);
      linkPToCells(predicate1);
      $scope.predicates.push(predicate1);
      initialisePredicate(predicate2);
      linkPToCells(predicate2);
      $scope.predicates.push(predicate2);
      initialisePredicate(predicate3);
      linkPToCells(predicate3);
      $scope.predicates.push(predicate3);
      initialisePredicate(predicate4);
      linkPToCells(predicate4);
      $scope.predicates.push(predicate4);
      //cellen
      $scope.cells["A3"] = "n1";
      evaluatePredicate("A", 3,"n1");
      $scope.cells["A4"] = "n2";
      evaluatePredicate("A", 4,"n2");
      $scope.cells["A5"] = "n3";
      evaluatePredicate("A", 5,"n3");
      $scope.cells["A6"] = "n4";
      evaluatePredicate("A", 6,"n4");
      $scope.cells["A10"] = "es";
      evaluatePredicate("A", 10,"es");
      $scope.cells["A11"] = "ns";
      evaluatePredicate("A", 11,"ns");
      $scope.cells["A12"] = "ls";
      evaluatePredicate("A", 12,"ls");

      $scope.cells["C3"] = "mon";
      evaluatePredicate("C", 3,"mon");
      $scope.cells["C4"] = "tue";
      evaluatePredicate("C", 4,"tue");
      $scope.cells["C5"] = "wed";
      evaluatePredicate("C", 5,"wed");
      $scope.cells["C6"] = "thu";
      evaluatePredicate("C", 6,"thu");
      $scope.cells["C7"] = "fri";
      evaluatePredicate("C", 7,"fri");
      $scope.cells["C8"] = "sat";
      evaluatePredicate("C", 8,"sat");
      $scope.cells["C9"] = "sun";
      evaluatePredicate("C", 9,"sun");

      $scope.constraints = "! n s d: Work(n,s,d) => Nurse(n) & Shift(s) & Day(d).\n\t\
! s d: Shift(s) & Day(d) => ?1 n : Work(n,s,d).\n\t\
! n s1 d: Work(n,s1,d) => ~ ? s2: s2 ~= s1 & Work(n,s2,d).\n\t\
! n: Nurse(n) => 1 =< #{d: Work(n,\"ls\",d)} < 3.\n\t\
! n: Nurse(n) => 1 =< #{d: Work(n,\"es\",d)} < 3.\n\t\
! n: Nurse(n) => 5 =< #{d s: Work(n,s,d)} < 7.\n\t"
      $scope.referencesEnabled = false;
      $scope.request();
      $scope.reloadRoute;

  }

  $scope.shortestPath = function() {
    resetVars();
    predicate1 = {name: "City", nbArguments: 1, length: 7, cell1: "A1", cell2: "A9", argument0: "name", argumentType0: "SomeText"};
    predicate2 = {name: "Roadway", nbArguments: 2, length: 15, cell1: "C1", cell2: "D17", argument0: "city", argumentType0: "SomeText",argument1: "city", argumentType1: "SomeText"};
    predicate3 = {name: "Path", nbArguments: 2, length: 15, cell1: "F1", cell2: "G17", argument0: "city", argumentType0: "SomeText", argument1: "city", argumentType1: "SomeText"};
    predicate4 = {name: "Reached", nbArguments: 3, length: 15, cell1: "I1", cell2: "K17", argument0: "city", argumentType0: "SomeText", argument1: "city", argumentType1: "SomeText", argument2: "#hops", argumentType2: "SomeInteger"};
    predicate5 = {name: "Reachable", nbArguments: 3, length: 15, cell1: "M1", cell2: "O17", argument0: "city", argumentType0: "SomeText", argument1: "city", argumentType1: "SomeText", argument2: "#hops", argumentType2: "SomeInteger"};

    initialisePredicate(predicate1);
    linkPToCells(predicate1);
    initialisePredicate(predicate2);
    linkPToCells(predicate2);
    initialisePredicate(predicate3);
    linkPToCells(predicate3);
    initialisePredicate(predicate4);
    linkPToCells(predicate4);
    initialisePredicate(predicate5);
    linkPToCells(predicate5);
    $scope.predicates.push(predicate1);
    $scope.predicates.push(predicate2);
    $scope.predicates.push(predicate3);
    $scope.predicates.push(predicate4);
    $scope.predicates.push(predicate5);


    $scope.cells["A3"] = "Antwerpen";
    evaluatePredicate("A", 3,"Antwerpen");
    $scope.cells["A4"] = "Mechelen";
    evaluatePredicate("A", 4,"Mechelen");
    $scope.cells["A5"] = "Herentals";
    evaluatePredicate("A", 5,"Herentals");
    $scope.cells["A6"] = "Aarschot";
    evaluatePredicate("A", 6,"Aarschot");
    $scope.cells["A7"] = "Leuven";
    evaluatePredicate("A", 7,"Leuven");
    $scope.cells["A8"] = "Brussel";
    evaluatePredicate("A", 8,"Brussel");
    $scope.cells["A9"] = "Tienen";
    evaluatePredicate("A", 9,"Tienen");

    $scope.cells["C3"] = "Antwerpen";
    evaluatePredicate("C", 3,"Antwerpen");
    $scope.cells["D3"] = "Herentals";
    evaluatePredicate("D", 3,"Herentals");
    $scope.cells["C4"] = "Mechelen";
    evaluatePredicate("C", 4,"Mechelen");
    $scope.cells["D4"] = "Aarschot";
    evaluatePredicate("D", 4,"Aarschot");
    $scope.cells["C5"] = "Antwerpen";
    evaluatePredicate("C", 5,"Antwerpen");
    $scope.cells["D5"] = "Mechelen";
    evaluatePredicate("D", 5,"Mechelen");
    $scope.cells["C6"] = "Herentals";
    evaluatePredicate("C", 6,"Herentals");
    $scope.cells["D6"] = "Mechelen";
    evaluatePredicate("D", 6,"Mechelen");
    $scope.cells["C7"] = "Aarschot";
    evaluatePredicate("C", 7,"Aarschot");
    $scope.cells["D7"] = "Leuven";
    evaluatePredicate("D", 7,"Leuven");
    $scope.cells["C8"] = "Aarschot";
    evaluatePredicate("C", 8,"Aarschot");
    $scope.cells["D8"] = "Tienen";
    evaluatePredicate("D", 8,"Tienen");
    $scope.cells["C9"] = "Leuven";
    evaluatePredicate("C", 9,"Leuven");
    $scope.cells["D9"] = "Brussel";
    evaluatePredicate("D", 9,"Brussel");
    $scope.cells["C10"] = "Brussel";
    evaluatePredicate("C", 10,"Brussel");
    $scope.cells["D10"] = "Tienen";
    evaluatePredicate("D", 10,"Tienen");
    $scope.cells["C11"] = "Aarschot";
    evaluatePredicate("C", 11,"Aarschot");
    $scope.cells["D11"] = "Herentals";
    evaluatePredicate("D", 11,"Herentals");
    $scope.cells["C12"] = "Mechelen";
    evaluatePredicate("C", 12,"Mechelen");
    $scope.cells["D12"] = "Leuven";
    evaluatePredicate("D", 12,"Leuven");
    $scope.cells["C13"] = "Leuven";
    evaluatePredicate("C", 13,"Leuven");
    $scope.cells["D13"] = "Tienen";
    evaluatePredicate("D", 13,"Tienen");

    $scope.cells["A11"] = "Start";
    evaluateNormalDefinition("A", 11,"Start");
    $scope.cells["A12"] = "Antwerpen";
    evaluateNormalDefinition("A", 12,"Antwerpen");
    $scope.cells["A13"] = "End";
    evaluateNormalDefinition("A", 13,"End");
    $scope.cells["A14"] = "Brussel";
    evaluateNormalDefinition("A", 14,"Brussel");

    $scope.predicateDef["Reached"] = "!x y: Reached(x,y,1) <- Path(x,y) & (Roadway(x,y) | Roadway(y,x)).\n\t\
!x y n1: Reached(x,y,n1) <- ?z n2: Path(x,z) & Reached(z,y,n2) & n2 = n1-1.\n\t"

    $scope.predicateDef["Reachable"] = "!x y:Reachable(x,y,1) <- Roadway(x,y) | Roadway(y,x).\n\t\
!x y z:Reachable(x,y,z) <- ?z2 z3: Reachable(x,z2,1) & Reachable(z2,y,z3) & z3 = z-1.\n\t"

    $scope.constraints = "! x y: Roadway(x,y) => City(x) & City(y).\n\t\
! x y z: Reached(x,y,z) => City(x) & City(y).\n\t\
! x y: Path(x,y) => City(x) & City(y).\n\n\t\
!x y:Path(x,y)=>Roadway(x,y) | Roadway(y,x).\n\t\
?x: Reached(CellT(12,\"A\"),CellT(14,\"A\"),x) & x = min{n: Reachable(CellT(12,\"A\"),CellT(14,\"A\"),n): n}.\n\n\t\
#{x: Path(CellT(12,\"A\"),x)} = 1.\n\t\
#{x: Path(x,CellT(12,\"A\"))} = 0.\n\t\
#{x: Path(CellT(14,\"A\"),x)} = 0.\n\t\
#{x: Path(x,CellT(14,\"A\"))} = 1.\n\n\t\
!x y: Path(x,y) & x ~= CellT(12,\"A\") => ?1 z: Path(z, x).\n\t\
!x y: Path(x,y) & y ~= CellT(14,\"A\") => ?1 z: Path(y,z).\n\n\t\
!x y: Path(x,y) => ~Path(y,x).\n\t\
!x y z: Path(x,y) => ~Reached(y,x,z).\n\t"

    $scope.referencesEnabled = false;
    $scope.request();
    $scope.reloadRoute;
  }


  resetVars = function(){
    $scope.spreadsheetText = [];
    $scope.spreadsheetNumber = [];
    $scope.IDPDef = {};
    var isNotEmptySpreadsheetPredicate = false;
    $scope.predicateDef = {};
    $scope.Defmodel = {};
    $scope.predicates = [];
    $scope.valueOfPredicateCell = {};
    $scope.cellsOfPredicates = []
    $scope.predicateLink = {};
    $scope.cellStyle = {};
    $scope.collSpan = {};
    $scope.idpCode = [window.idpCode];
    $scope.constraints = "";
    $scope.requestSend = "Idle";
    $scope.numberOfModels = 0;
    $scope.modelAsked = 1;
    $scope.multipleModelsEnabled = true;
    $scope.referencesEnabled = true;
    var waitingList = {};
  }


  evaluateNormalDefinition = function(column,row,exp) {
    var isNumber = Number(exp);

    if (exp.length == 0 || !exp.trim()) {
      $scope.IDPDef[column+row] = undefined;
      $scope.textCells[column+row] = undefined;
      if ($.inArray(column+row, $scope.spreadsheetText) != -1)
        deleteElementofArray(column+row, $scope.spreadsheetText);
      if ($.inArray(column+row, $scope.spreadsheetNumber) != -1)
        deleteElementofArray(column+row, $scope.spreadsheetNumber);
    }

    else if(exp.search(/=/) == 0) {
      if (/[A-Z]+\d+/g.test(exp.substring(1))) {
        var changedExp = exp.substring(1).replace(/[A-Z]+\d+/g, function(ref) {
          if($.inArray(ref, $scope.spreadsheetNumber) != -1) {
            waitingList[ref] = [column,row];
            return"Cell(" + /\d+/.exec(ref) + ","  + "\"" + /[A-Z]+/.exec(ref) + "\"" + ")";
          }
          else if($.inArray(ref, $scope.spreadsheetText) != -1) {
            waitingList[ref] = [column,row];
            return"CellT(" + /\d+/.exec(ref) + ","  + "\"" + /[A-Z]+/.exec(ref) + "\"" + ")";
          }
          else {//We weten nog niet of het naar een getal of text verwijst dus we zetten het in een wachtlijst en zetten het voorlopig op getal
            waitingList[ref] = [column,row]; //moet bij alles, sebiet zo regelen
            return"Cell(" + /\d+/.exec(ref) + ","  + "\"" + /[A-Z]+/.exec(ref) + "\"" + ")";
          }
        });


        if ($.inArray(/[A-Z]+\d+/.exec(exp.substring(1))[0],$scope.spreadsheetText) != -1) {
          $scope.IDPDef[column+row] = "CellT(" + row + "," + "\"" + column + "\"" + ")" + " = " + changedExp + "."  ;
          if ($.inArray(column+row, $scope.spreadsheetText) == -1) {
            if ($.inArray(column+row, $scope.spreadsheetNumber) != -1)
              deleteElementofArray(column+row, $scope.spreadsheetNumber);
            $scope.spreadsheetText.push(column+row);
          }
        }
        else {

          $scope.IDPDef[column+row] = "Cell(" + row + "," + "\"" + column + "\"" + ")" + " = " + changedExp + "."  ;
          $scope.textCells[column+row] = undefined;
          if ($.inArray(column+row, $scope.spreadsheetNumber) == -1) {
            if ($.inArray(column+row, $scope.spreadsheetText) != -1)
              deleteElementofArray(column+row, $scope.spreadsheetText);
            $scope.spreadsheetNumber.push(column+row);

          }
        }

      }
      else {
        $scope.textCells[column+row] = undefined;
        if ($.inArray(column+row, $scope.spreadsheetNumber) == -1) {
          if ($.inArray(column+row, $scope.spreadsheetText) != -1)
            deleteElementofArray(column+row, $scope.spreadsheetText);
          $scope.spreadsheetNumber.push(column+row);
        }
        $scope.IDPDef[column+row] = "Cell(" + row + "," + "\"" + column + "\"" + ")" + " = " + exp.substring(1) + "."  ;
      }
    }
    else if (isNaN(isNumber)) {
      if ($.inArray(column+row, $scope.spreadsheetText) == -1) {
        if ($.inArray(column+row, $scope.spreadsheetNumber) != -1)
          deleteElementofArray(column+row, $scope.spreadsheetNumber);
        $scope.spreadsheetText.push(column+row);
      }
      $scope.IDPDef[column+row] = "CellT(" + row + "," + "\"" + column + "\"" + ")" + " = \"" + exp + "\"."  ;
      $scope.textCells[column+row] = exp;
    }
    else {
      $scope.IDPDef[column+row] = "Cell(" + row + "," + "\"" + column + "\"" + ")" + " = " + exp + "."  ;
      if ($.inArray(column+row, $scope.spreadsheetNumber) == -1) {
        if ($.inArray(column+row, $scope.spreadsheetText) != -1)
          deleteElementofArray(column+row, $scope.spreadsheetText);
        $scope.spreadsheetNumber.push(column+row);
      }
    }
  }

  evaluatePredicate = function(column,row,exp) {
    if(exp.length == 0 || !exp.trim())
      $scope.valueOfPredicateCell[column+row] = undefined;
    else
      $scope.valueOfPredicateCell[column+row] = exp;
  }

  $scope.change = function(column,row) {
    var exp = $scope.cells[column+row];

    if (exp == null)
      return;

    if($.inArray(column+row, $scope.cellsOfPredicates) != -1 )
      evaluatePredicate(column,row,exp);
    else
      evaluateNormalDefinition(column,row,exp);

    if(isEmptyObject($scope.IDPDef))
      isNotEmptySpreadsheetPredicate = false;
    else
      isNotEmptySpreadsheetPredicate = true;

    if (waitingList.hasOwnProperty(column+row) && waitingList[column+row][0] != undefined) {
      $scope.change(waitingList[column+row][0],waitingList[column+row][1]);

    }

    $scope.request();
  };

  function isEmptyObject(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop) &&  obj[prop] != undefined )
            return false;
    }
    return true;
}

  $scope.prevModel = function() {
    if( $scope.modelAsked > 1) {
      $scope.modelAsked = $scope.modelAsked - 1;
      $scope.request();
    }
  }

  $scope.nextModel = function() {
    if( $scope.modelAsked < $scope.numberOfModels) {
      $scope.modelAsked = $scope.modelAsked + 1;
      $scope.request();
    }
  }

  modelAskedHandler = function() {
    if ($scope.multipleModelsEnabled)
      return $scope.modelAsked;
    else
      return 0;
  }

  $scope.request = function() {
    $scope.requestSend = "Request send to IDP";
    var code =  PutLinksIncode();
    var r = {
      "code": PutLinksIncode(),//"code": putDefsInCode(), //The IDP source code
      "inference": 2, //Means the main() method will be called
      //"eInput" : JSON.stringify(isNotEmptySpreadsheetPredicate) //geef als argument mee of predicaat Spreadsheet al dan niet moet teruggegeven worden
      "eInput" : JSON.stringify(modelAskedHandler())
    };

    $.ajax({
      type: 'POST',
      url: "https://idp.cs.kuleuven.be/idp/eval",
      data: JSON.stringify(r),
    }).done(function(result) {
          if (result.stdout == "") {
            $scope.requestSend = "error detected or resource limits reached";
            window.alert(result.stderr);
            //console.log(result.stderr);
            $scope.reloadRoute();
          }
          else {
            var model = JSON.parse(result.stdout);

            if (model == "Unsat") {
              $scope.requestSend = "Unsatisfiable model";
              $scope.reloadRoute();
              $scope.numberOfModels = 0;
            }

            else {
              $scope.numberOfModels = model.splice(0,1)[0];
              if ($scope.modelAsked > $scope.numberOfModels)
                $scope.modelAsked = $scope.numberOfModels;
                //$scope.Defmodel = JSON.parse(result.stdout);

                else if (jQuery.isEmptyObject(model)) {
                  $scope.requestSend = "Empty model returned";
                  $scope.reloadRoute();
                }
                else {
                  //console.log(code);
                  $scope.changeValues(model);
                  $scope.reloadRoute();
                  $scope.requestSend = "Idle";
                }
              }
            }

          });

  }


  $scope.reloadRoute = function() {
   $route.reload();
  }

  isCell = function(cell) {
    if (cell.replace(/[A-Z]+\d+/g,"") == "")
      return true;
    return false;
  }

  $scope.changeValues = function(model) {
    $scope.Defmodel = {};
    //predicaten initialiseren
    for (i in $scope.predicates)
      initialisePredicate($scope.predicates[i]);
    //  voor als er een waarde alleen is

    for (i in $scope.cellsOfPredicates)
      $scope.Defmodel[$scope.cellsOfPredicates[i]] = $scope.valueOfPredicateCell[$scope.cellsOfPredicates[i]];


    name = "";
    instance = 0;
    for (var i = 0; i < model.length; i++) {
        var temp = model[i];
        if (temp.hasOwnProperty("cell")) {//Mss in de plaats kijken of cell in het object zit
          //if (isNotEmptySpreadsheetPredicate)
            $scope.Defmodel[temp.cell] = temp.value;

        }
        else {
          if(name != temp.name)
            instance = 0;
          else
            instance++;
          name = temp.name;
          cells = $scope.predicateLink[name];
          len = Object.keys(temp).length - 1;
          for (var j = 0; j < len; j++)
            $scope.Defmodel[cells[instance * len + j]] = temp["value"+ (j)];
        }
     }

     //Dit erachter zodat de volgorde niet veranderd

     for (i in $scope.cellsOfPredicates)
      if ($scope.valueOfPredicateCell[$scope.cellsOfPredicates[i]] != undefined) {
       $scope.Defmodel[$scope.cellsOfPredicates[i]] = $scope.valueOfPredicateCell[$scope.cellsOfPredicates[i]];
     }

  }

  //Dit is de functie die je moet oproepen in ng-bind van div output om de spreadsheet door javascript te laten evalueren
  $scope.compute = function(cell) {
    var exp = $scope.cells[cell];
    var isNumber = Number(exp);
    if(exp.search(/=/) == 0) {
      return $parse(process(exp.substring(1)))($scope);
    }
    else if (isNaN(isNumber)) {
      return exp;
    }
    else {
      return isNumber;
    }
  };

  $scope.computeTest = function(cell) {
    var exp = $scope.cells[cell];
    //return cell;
    if(exp.search(/=/) == 0 && ! (/ggg/.test($scope.idpCode[0]))) {
      $scope.idpCode[0] = $scope.idpCode[0] + " ggg";
    }
    return;
  };


  transform = function(exp,row,column) {
    var changedExp = exp.replace(/[A-Z]+\d+/g, function(ref) {
      return "Cell(" + /[A-Z]+/.exec(ref) + "," +  /\d+/.exec(ref) + ")";
    });
    return "Cell(" + row + "," + column + ")" + " <- " + changedExp;
  }

  $scope.computeIDP = function(column, row) {
    var exp = $scope.cells[column+row];
    var isNumber = Number(exp);
    if(exp == null )
      return;

    if( exp.search(/=/) == 0) {
      for (var i = 0; i < $scope.Defmodel.length; i++) {
        var model = $scope.Defmodel[i];
        if (model.cell == column+row)
            return model.value;
        //if (model[0] == row && model[1] == column)
        //      return model[2];
      }
    }
    else if (isNaN(isNumber)) {
      return exp;
    }
    else {
      //plaats in de IDP def dat deze cell deze waarde heeft
      return isNumber;
    }

  }

  initialisePredicate = function(predicate) {
    var cell1 = predicate.cell1;
    var cell2 = predicate.cell2;
    //Dit is voorlopig, moet ook bij dat er meerdere Letters na elkaar kunnen komen
    var rowCell1 = cell1.substring(1);
    var rowCell2 = cell2.substring(1);
    var columnLength = cell2.charCodeAt(0) - cell1.charCodeAt(0);
    var rowLength = rowCell2 - rowCell1;
    //Predicaat in de cellen initialiseren
      $scope.Defmodel[cell1] = predicate.name;
    for (var i = 0; i <= columnLength; i++) {
      $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) + rowCell1] = "#595959";
      $scope.collSpan[String.fromCharCode(cell1.charCodeAt(0) + i) + rowCell1] = true;
      $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = "#b3b3b3";
      $scope.collSpan[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = true;
      $scope.Defmodel[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = predicate["argument" + i];
    }

    for (var j = parseInt(rowCell1) + 1; j <= rowLength + parseInt(rowCell1) - 1; j++) {
      for (var i = 0; i <= columnLength; i++) {
        $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) +( j + 1)] = "#e6e6e6";
      }
    }

  }

  deletePredicate = function(Newpredicate) {
    for (i in $scope.predicates)
      if($scope.predicates[i].name == Newpredicate.predicate)
        predicate = $scope.predicates[i];
    var cell1 = predicate.cell1;
    var cell2 = predicate.cell2;
    //Dit is voorlopig, moet ook bij dat er meerdere Letters na elkaar kunnen komen
    var rowCell1 = cell1.substring(1);
    var rowCell2 = cell2.substring(1);
    var columnLength = cell2.charCodeAt(0) - cell1.charCodeAt(0);
    var rowLength = rowCell2 - rowCell1;
    //Predicaat in de cellen initialiseren
    $scope.Defmodel[cell1] = undefined;
    for (var i = 0; i <= columnLength; i++) {
      $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) + rowCell1] = "white";
      $scope.collSpan[String.fromCharCode(cell1.charCodeAt(0) + i) + rowCell1] = false;
      $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = "#white";
      $scope.collSpan[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = false;
      $scope.Defmodel[String.fromCharCode(cell1.charCodeAt(0) + i) + (parseInt(rowCell1) + 1)] = undefined;
    }

    for (var j = parseInt(rowCell1) + 1; j <= rowLength + parseInt(rowCell1) - 1; j++) {
      for (var i = 0; i <= columnLength; i++) {
        $scope.cellStyle[String.fromCharCode(cell1.charCodeAt(0) + i) +( j + 1)] = "white";
        delete $scope.valueOfPredicateCell[String.fromCharCode(cell1.charCodeAt(0) + i) +( j + 1)];
      }
    }

    if($scope.predicateDef[predicate.name] != undefined)
      $scope.predicateDef[predicate.name] = undefined;
    deleteCells(Newpredicate.predicate);

    delete $scope.predicateLink[Newpredicate.predicate];

    //deleteTypes(Newpredicate.predicate);
    var index;
    for (i in $scope.predicates) {
      if ($scope.predicates[i].name == Newpredicate.predicate)
        index = $scope.predicates.indexOf($scope.predicates[i]);
      //else
        //checkTypes($scope.predicates[i]);
    }
    $scope.predicates.splice(index,1);
    $scope.request();
  }

  deleteCells = function(predicate) {
    var cells = $scope.predicateLink[predicate]
    for ( i in cells) {
      cell = cells[i];
      isIn = false;
      for (j in $scope.predicateLink)
        if (j != predicate)
          if ($.inArray(cell,$scope.predicateLink[j]))
            isIn = true;
      if (isIn == false) {
        index = $scope.cellsOfPredicates.indexOf(cell);
        $scope.cellsOfPredicates.splice(index,1);
      }
    }
  }

/*
  deleteTypes = function(predicateName) {
    for (i in $scope.predicates)
      if($scope.predicates[i].name == predicateName)
          predicate = $scope.predicates[i];
          nb = predicate.nbArguments;
          for (var j = 0; j < nb; j++) {
            index = $scope.types.indexOf(predicate["argumentType" + j]);
            $scope.types.splice(index,1);
          }
  }
  */

  deleteElementofArray= function(element,array) {
        index = array.indexOf(element);
        array.splice(index,1);
  }

  validatePredicate = function(predicate) {
    var cell1 = predicate.cell1;
    var cell2 = predicate.cell2;
    var nbArguments = predicate.nbArguments;
    var name = predicate.name;
    var columnLength = cell2.charCodeAt(0) - cell1.charCodeAt(0);
    if(name == null || name == "") {
      window.alert("A predicate must have a name");
      return false;
    }
    if(cell1 == null || cell1 == "" || cell1.replace((/[A-Z]+\d+/.exec(cell1)),"") != "") {
      window.alert("cell1 is no valid cell");
      return false;
    }
    if(cell2 == null || cell2 == "" || cell2.replace((/[A-Z]+\d+/.exec(cell2)),"") != "") {
      window.alert("cell2 is no valid cell");
      return false;
    }
    if(columnLength + 1 != nbArguments) {
      window.alert("The number of arguments doesn't correspond to the number of columns in the given range");
      return false;
    }
    for(var i = 0; i < nbArguments; i++) {
      name = predicate["argument" + i];
      type = predicate["argumentType" + i];
      if(name == null || name == "") {
        window.alert("Argument name is not given");
        return false;
      }
      if(type == null || type == "") {
        window.alert("Argument type is not given");
        return false;
      }
    }
    return true;
  }

  validateDefinition = function(data) {
    predicate = data.predicate;
    if(predicate != null) {
      return true;
    }
  }

  linkPToCells = function (predicate) {
    var name = predicate.name;
    var cell1 = predicate.cell1;
    var cell2 = predicate.cell2;
    var allCells = [];
    var count = 0;
    var nb = predicate.nbArguments;
    for (var j = parseInt(cell1.substring(1)) + 2; j <= parseInt(cell2.substring(1)); j++) {
      for (var i = cell1.charCodeAt(0); i <= cell2.charCodeAt(0); i++ ) {
        allCells.push(String.fromCharCode(i) + j);
        if (predicate["argumentType" + count] == "SomeText") {
          if ($.inArray(String.fromCharCode(i) + j, $scope.spreadsheetNumber) != -1)
            deleteElementofArray(String.fromCharCode(i) + j, $scope.spreadsheetNumber);
          $scope.spreadsheetText.push(String.fromCharCode(i) + j);
          //mss ne if met test of het er inzat als dees door change ook wordt op geroepen
        }
        else {
          if ($.inArray(String.fromCharCode(i) + j, $scope.spreadsheetText) != -1)
            deleteElementofArray(String.fromCharCode(i) + j, $scope.spreadsheetText);
          $scope.spreadsheetNumber.push(String.fromCharCode(i) + j);
        }
        count++;
        if ($.inArray(String.fromCharCode(i) + j, $scope.cellsOfPredicates) == -1)
            $scope.cellsOfPredicates.push(String.fromCharCode(i) + j);
      }
      count = 0;
    }
    $scope.predicateLink[name] = allCells;
  }

/*
  checkTypes = function(predicate) {
    for (var i = 0; i < predicate.nbArguments; i++) {
      if ($.inArray(predicate["argumentType" + i], $scope.types) == -1)
        $scope.types.push(predicate["argumentType" + i]);
    }
  }
  */

  $scope.openEditor = function () {
      modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'MyIDPModal.html',
      controller: 'ModalInstanceEditorCtrl',
      size: 'lg',
      resolve : {
        //idpCode : function(){return [putDefsInCode()] }
        idpCode : function(){return [PutLinksIncode()] }
        //idpCode : function(){return [$scope.idpCode[0] + JSON.stringify($scope.IDPDef)]}
      }
    });
  };

  $scope.openConstraintsEditor = function () {
      modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'MyConstraintsModal.html',
      controller: 'ModalInstanceConstraintsEditorCtrl',
      size: 'lg',
      resolve : {
        constraints : function(){return $scope.constraints }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      //Do Something  with the values
      $scope.constraints = selectedItem;
      $scope.request();

    }, function () {
          //modal was dismissed
       });
  };

  $scope.addDefinitiontoPredicate = function() {
    modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'MyDefinitionModal.html',
      controller: 'ModalInstanceDefinitionCtrl',
      size: 'lg',
      resolve : {
        predicates : function(){return $scope.predicates;},
        def: function(){return $scope.predicateDef;}
      }
    });

    modalInstance.result.then(function (selectedItem) {
      //Do Something  with the values
      for (var i in selectedItem) {
        if (i != "predicate") {
            $scope.predicateDef[i] = selectedItem[i];
            //console.log("Def send");
            //console.log(PutLinksIncode());
            //$scope.request();
        }
      }
        $scope.request();

    }, function () {
          //modal was dismissed
       });
  };

  $scope.openAddPredicate = function () {
      modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'MyPredicateModal.html',
        controller: 'ModalInstancePredicateCtrl',
        size: 'lg',
      });

    modalInstance.result.then(function (selectedItem) {
      //Do Something with the values
      if (validatePredicate(selectedItem)) {
        initialisePredicate(selectedItem);
        linkPToCells(selectedItem);
        //checkTypes(selectedItem);
        $scope.predicates.push(selectedItem);
        $scope.request();
      }

    }, function () {
          //modal was dismissed
       });

    };

    makeObjectOfPredicates = function() {
      //clone
      objectP = {};
      predicates = (JSON.parse(JSON.stringify($scope.predicates)));
      for (i in predicates) {
        objectP[predicates[i].name] = predicates[i];
        objectP[predicates[i].name]["predicate"] = predicates[i].name;
      }
      return objectP;
    }

    $scope.openEditPredicate = function() {
      modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'MyEditPredicateModal.html',
        controller: 'ModalInstanceEditPredicateCtrl',
        size: 'lg',
        resolve : {
          predicates : makeObjectOfPredicates(),
          listPredicates: function() {return (JSON.parse(JSON.stringify($scope.predicates)))}
        }
      });

      modalInstance.result.then(function (selectedItem) {
        //Do Something with the values
        for (var i in selectedItem) {
          if (i != "undefined" && i != "predicate" && validateDefinition(selectedItem[i])) {
            if (validatePredicate(selectedItem[i])) {
              //Dit moet nog veranderen
              //Verwijder dit predicaat
              var temp = $scope.predicateDef[selectedItem[i].predicate];
              deletePredicate(selectedItem[i]);
              if (temp != undefined)
                $scope.predicateDef[selectedItem[i].predicate] = temp;
              initialisePredicate(selectedItem[i]);
              linkPToCells(selectedItem[i]);
              //checkTypes(selectedItem);
              $scope.predicates.push(selectedItem[i]);
              cells = $scope.predicateLink[selectedItem[i].name];
              for (i in cells) {
                row = cells[i].substring(1);
                column = cells[i].substring(0,1);
                $scope.change(column, row);
              }
            }
          }
        }
        $scope.request();

      }, function () {
            //modal was dismissed
         });
    };


    $scope.openDeletePredicate = function() {
      modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'MydeletePredicateModal.html',
        controller: 'ModalInstanceDeletePredicateCtrl',
        size: 'sm',
        resolve : {
          predicates : function(){return $scope.predicates;}
        }
      });

      modalInstance.result.then(function (selectedItem) {
        //Do Something  with the values
        if (validateDefinition(selectedItem)) {
            deletePredicate(selectedItem);
            $scope.request();
        }

      }, function () {
            //modal was dismissed
         });
    };


  });



app.controller('ModalInstanceEditorCtrl', function ($scope, $uibModalInstance, idpCode) {

 //$scope.$on('modal.closing', init);

 $scope.idpCode = idpCode;

 //geeft niets omdat window.idpCode niet meer bestaat. En dit nu is opgesplitst
 $scope.ok = function(){
   $scope.idpCode[0] = window.idpCode;
 };

 $scope.cancel = function () {
   $uibModalInstance.dismiss();
 };

 $scope.codemirrorLoaded = function(_editor){
   _editor.setOption('lineWrapping', true);
   _editor.setOption('lineNumbers', true);
   _editor.setOption('mode', 'idp');
   setTimeout(function() {_editor.refresh()}, 100);
   window.cm = _editor
 }

});

app.controller('ModalInstanceConstraintsEditorCtrl', function ($scope, $uibModalInstance, constraints) {

 //$scope.$on('modal.closing', init);

 $scope.constraints = constraints;

 //geeft niets omdat window.idpCode niet meer bestaat. En dit nu is opgesplitst
 $scope.ok = function(){
   $uibModalInstance.close($scope.constraints);
 };

 $scope.cancel = function () {
   $uibModalInstance.dismiss();
 };

 $scope.codemirrorLoaded = function(_editor){
   _editor.setOption('lineWrapping', true);
   _editor.setOption('lineNumbers', true);
   _editor.setOption('mode', 'idp');
   setTimeout(function() {_editor.refresh()}, 100);
   window.cm = _editor
 }

});

app.controller('ModalInstancePredicateCtrl', function ($scope, $uibModalInstance) {
  $scope.argument = "argument";
  $scope.argumentType = "argumentType";
  $scope.formData = {nbArguments: 2, cell1: "A1", cell2: "B7", length: 5};

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };

  $scope.changeCell1 = function() {
    $scope.nbArgmumentChange();
    $scope.lengthChange();
  }

  $scope.nbArgmumentChange = function() {
     var nb = $scope.formData.nbArguments;
     cell1 = $scope.formData.cell1;
     cell2 = $scope.formData.cell2;
     column = String.fromCharCode(cell1.charCodeAt(0) + nb - 1)
     row = cell2.substring(1);
     $scope.formData.cell2 = column+row;
  };

  $scope.changeCell2 = function() {
    var nb = $scope.formData.nbArguments;
    cell1 = $scope.formData.cell1;
    cell2 = $scope.formData.cell2;
    column = String.fromCharCode(cell2.charCodeAt(0) - nb + 1)
    row = cell1.substring(1);
    $scope.formData.cell1 = column+row;
  };

  $scope.lengthChange = function() {
    var ln = $scope.formData.length;
    cell1 = $scope.formData.cell1;
    cell2 = $scope.formData.cell2;
    column = cell2.substring(0,1);
    row = ln + 1 + parseInt(cell1.substring(1));
    $scope.formData.cell2 = column+row;
  };

  $scope.submit = function () {
    $uibModalInstance.close($scope.formData);
  };

  $scope.range = function(n) {
        return new Array(n);
    };
});

app.controller('ModalInstanceDefinitionCtrl', function ($scope, $uibModalInstance, predicates,def) {
  $scope.predicates = predicates;
  $scope.formData = (JSON.parse(JSON.stringify(def)));
  //$scope.formData = {};

//moet via html
/*
  $scope.changePredicate = function() {
    predicate = $scope.formData.predicate;
    $scope.formData.definition = def[predicate];
  };
  */

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };

  $scope.submit = function () {
    $uibModalInstance.close($scope.formData);
  };

  $scope.codemirrorLoaded = function(_editor){
    _editor.setOption('lineWrapping', true);
    _editor.setOption('lineNumbers', true);
    _editor.setOption('mode', 'idp');
    setTimeout(function() {_editor.refresh()}, 100);
    window.cm = _editor
  }
});

app.controller('ModalInstanceEditPredicateCtrl', function ($scope, $uibModalInstance, predicates, listPredicates) {
  $scope.argument = "argument";
  $scope.argumentType = "argumentType";
  // cloned het object predicates. Anders worden beide aangepast.
  //$scope.predicates = (JSON.parse(JSON.stringify(predicates)));
  $scope.listOfPredicates = (JSON.parse(JSON.stringify(listPredicates)));
  $scope.predicates = (JSON.parse(JSON.stringify(predicates)));
  $scope.formData = (JSON.parse(JSON.stringify(predicates)));


  $scope.changePredicate = function() {
    predicate = $scope.formData.predicate;
    for (i in $scope.predicates) {
      if (i == predicate) {
          //$scope.formData = (JSON.parse(JSON.stringify($scope.predicates[i]))); moet nu via html
          $scope.formData.predicate = predicate;
          break;
      }
    }
  };

  $scope.changeCell1 = function() {
    $scope.nbArgmumentChange();
    $scope.lengthChange();
  };

  $scope.lengthChange = function() {
    pr = $scope.formData.predicate;
    var ln = $scope.formData[pr].length;
    cell1 = $scope.formData[pr].cell1;
    cell2 = $scope.formData[pr].cell2;
    column = cell2.substring(0,1);
    row = ln + 1 + parseInt(cell1.substring(1));
    $scope.formData[pr].cell2 = column+row;
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };

  $scope.nbArgmumentChange = function() {
     pr = $scope.formData.predicate;
     var nb = $scope.formData[pr].nbArguments;
     cell1 = $scope.formData[pr].cell1;
     cell2 = $scope.formData[pr].cell2;
     column = String.fromCharCode(cell1.charCodeAt(0) + nb - 1)
     row = cell2.substring(1);
     $scope.formData[pr].cell2 = column+row;
  };

  $scope.changeCell2 = function() {
    pr = $scope.formData.predicate;
    var nb = $scope.formData[pr].nbArguments;
    cell1 = $scope.formData[pr].cell1;
    cell2 = $scope.formData[pr].cell2;
    column = String.fromCharCode(cell2.charCodeAt(0) - nb + 1)
    row = cell1.substring(1);
    $scope.formData[pr].cell1 = column+row;
  };

  $scope.submit = function () {
    $uibModalInstance.close($scope.formData);
  };

  $scope.range = function(n) {
        return new Array(n);
    };
});


app.controller('ModalInstanceDeletePredicateCtrl', function ($scope, $uibModalInstance, predicates) {
  $scope.predicates = predicates;
  $scope.formData = {};

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };

  $scope.delete = function () {
    $uibModalInstance.close($scope.formData);
  };

});
